import { AppPage } from './app.po';
import { element, by } from 'protractor';
import { browser } from 'protractor';
import { NgSelectMultipleOption, SelectMultipleControlValueAccessor } from '@angular/forms/src/directives';
import { isPending } from 'q';
import { protractor } from 'protractor';
describe('workspace-project App', () => {
  browser.get('http://localhost:4200');
  it('should be home view', () => {

    browser.ignoreSynchronization = true;

    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');
  });


  it('should change to Register view', () => {
    var register = element(by.id('register'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/signup');
  });
  it('should change to Login view', () => {
    var register = element(by.id('login'));

    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/signin');
  });
  it('should change to all views', () => {
    var register = element(by.id('register'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/signup');
    var register = element(by.id('login'));

    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/signin');
  });
  it('should register', async () => {

    var login = element(by.id('register'));

    login.click();
    var nome = element(by.id('name'));
    nome.sendKeys('abcd');
    var email = element(by.id('email'));
    email.sendKeys('116090wsaaasassassasasse0@isep.ipp.pt');
    var password = element(by.id('password'));
    password.sendKeys('2ekgjgRwc9');
    var checkbox = element(by.id('checkbox'));
    checkbox.click();
    var login = element(by.id('register'));
    login.click();
    var EC = protractor.ExpectedConditions;
    // Wait for new page url to contain efg
    browser.wait(EC.urlIs('http://localhost:4200/signin'), 10000000);

    expect(browser.driver.getCurrentUrl()).toEqual('http://localhost:4200/signin');






  });
  it('should log in as a costumer', async () => {

    var login = element(by.id('login'));

    login.click();
    var email = element(by.id('email'));
    email.sendKeys('116090wsaaasassassasasse0@isep.ipp.pt');
    var password = element(by.id('password'));
    password.sendKeys('2ekgjgRwc9');
    var login = element(by.id('login'));
    login.click();
    var EC = protractor.ExpectedConditions;
    // Wait for new page url to contain efg
    await browser.wait(EC.urlIs('http://localhost:4200/'), 10000000);
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');

  });
  it('should change to product view', () => {
    var register = element(by.id('products'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/products');
  });

  //testes sobre products
  it('should change to ShoppingList view', () => {
    var register = element(by.id('Shopping List'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/shopping-list');
  });

  it('should change to OrderHistory view', () => {
    var register = element(by.id('Order History'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/order-history');
  });




  //ultimo teste de costumers! 
  it('should logout', () => {
 
    var register = element(by.id('logout'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');
  });


  it('should log in as a contentManager', async () => {

    var login = element(by.id('login'));

    login.click();
    var email = element(by.id('email'));
    email.sendKeys('contentManager@gmail.com');
    var password = element(by.id('password'));
    password.sendKeys('contentManager');
    var login = element(by.id('login'));
    login.click();
    var EC = protractor.ExpectedConditions;
    // Wait for new page url to contain efg
    await browser.wait(EC.urlIs('http://localhost:4200/'), 10000000);
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');

  });
  it('should change to Catalog view', () => {
    var register = element(by.id('Catalog'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/catalog');
  });
  it('should change to collection view', () => {
    var register = element(by.id('Collection'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/collection');
  });
  it('should change to category view', () => {
    var register = element(by.id('Categories'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/category');
  });
  it('should change to material view', () => {
    var register = element(by.id('Material'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/material');
  });
  
  //ultimo teste de costumer Manager! 
  it('should logout', () => {
    var register = element(by.id('logout'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');
  });
  it('should log in as a orderManager', async () => {

    var login = element(by.id('login'));

    login.click();
    var email = element(by.id('email'));
    email.sendKeys('orderManager@isep.ipp.pt');
    var password = element(by.id('password'));
    password.sendKeys('orderManager');
    var login = element(by.id('login'));
    login.click();
    var EC = protractor.ExpectedConditions;
    // Wait for new page url to contain efg
    await browser.wait(EC.urlIs('http://localhost:4200/'), 10000000);
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');

  });
  it('should change to calc circuit view', () => {
    var register = element(by.id('Calc Circuit'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/calc-circuit');
  });
  it('should change to assign orders view', () => {
    var register = element(by.id('Assign Orders'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/assign-orders-factories');
  });
  //ultimo teste de costumer Manager! 
  it('should logout', () => {
    var register = element(by.id('logout'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');
  });
  it('should log in as a clericalWorker', async () => {

    var login = element(by.id('login'));

    login.click();
    var email = element(by.id('email'));
    email.sendKeys('clericalWorker@isep.ipp.pt');
    var password = element(by.id('password'));
    password.sendKeys('clericalWorker');
    var login = element(by.id('login'));
    login.click();
    var EC = protractor.ExpectedConditions;
    // Wait for new page url to contain efg
    await browser.wait(EC.urlIs('http://localhost:4200/'), 10000000);
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');

  });
  it('should change to assign orders view', () => {
    var register = element(by.id('Materials and Finishes'));


    register.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/materialFinish');
  });

});
